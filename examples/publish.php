<?php

require("../phpMQTT.php");

$message = $argv[1] ?: '';
	
$mqtt = new phpMQTT("localhost", 1883, "Unique publisher");

if ($mqtt->connect()) {
    // $mqtt->publish("testCLI/publishtest", $message, 0);
    $mqtt->publish("messages/unsaved/chat", $message, 0);
	$mqtt->close();
}
