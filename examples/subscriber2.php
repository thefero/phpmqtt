<?php
require("../phpMQTT.php");

$mqtt = new phpMQTT("localhost", 1883, "Unique listener 2");

if (!$mqtt->connect()) {
    exit(1);
}

$topics['testCLI/#'] = [
    "qos" => 0,
    "function" => "processMsg"
];
$mqtt->subscribe($topics, 0);

while($mqtt->proc()){ usleep(20000); }
$mqtt->close();

function processMsg($topic, $msg) {
    echo "$msg\n";
}